def transcribe(line, ipa):
    transcription = ""
    for letter in line:
        if letter in ipa.keys():
            transcription = transcription + ipa[letter]
        else:
            transcription = transcription + letter
    return transcription

ipa = {"ა": "ɑ", "ბ": "b", "გ": "g", "დ": "d", "ე": "ɛ", "ვ": "v", "ზ": "z", "ჱ": "ɛj", "თ": "tʰ", "ი": "ɪ", "კ": "kʼ", "ლ": "l", "მ": "m", "ნ": "n", "ჲ": "j", "ო": "ɔ", "პ": "pʼ", "ჟ":"ʒ", "რ": "r", "ს": "s", "ტ": "tʼ", "ჳ": "wi", "უ": "u", "ფ": "pʰ", "ქ": "kʰ", "ღ": "ɣ", "ყ": "qʼ", "შ": "ʃ", "ჩ": "tʃ", "ც": "ts", "ძ": "dz", "წ": "tsʼ", "ჭ": "tʃʼ", "ხ": "x", "ჴ": "q", "ჯ": "dʒ", "ჰ":"h", "ჵ": "hɔɛ"}

with open("source_text.txt", "r", encoding = "utf-8") as source, open("transcription.txt", "w", encoding = "utf-8") as result:
    for line in source:
        transcription = transcribe(line, ipa)
        result.write(transcription)

