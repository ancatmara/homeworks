def transcribe(line, alph):
    """
    :param line: a string of characters
    :param alph: a dictionary with source symbols as keys and result symbols as values
    :return: a string of characters
    """
    transcription = ""
    for char in line:
        if char in alph.keys():
            transcription = transcription + alph[char]
        else:
            transcription = transcription + char
    return transcription

with open("amhar.tsv", "r", encoding = "utf-8") as table:
    cons = []
    voc = []
    alph = {}
    c = 0
    for line in table:
        line = line.strip("\n")
        line = line.split("\t")
        v = 0
        for char in line:
            char = char.strip(" ")
            if c == 0 and v != 0:
                voc.append(char)
            if c != 0 and v == 0:
                cons.append(char)
            if c != 0 and v != 0:
                alph[char] = cons[c-1] + voc[v-1]
            v += 1
        c += 1

with open("amhar_source.txt", "r", encoding = "utf-8") as text, open("amhar_result.txt", "w", encoding = "utf-8") as result:
#    for char in alph:
#        result.write(char + " " + alph[char] + "\n")
    for line in text:
        transcription = transcribe(line, alph)
        result.write(transcription)